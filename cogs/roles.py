# -*- coding: utf-8 -*-

import discord
from discord.ext import commands


class Roles(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def get_info(self, payload):
        channel = self.bot.get_channel(payload.channel_id)
        member = channel.guild.get_member(payload.user_id)
        message = await channel.fetch_message(payload.message_id)
        db_role = await self.bot.db.fetchval("SELECT role FROM role_reaction WHERE reaction =  $1;", str(payload.emoji))
        return channel, member, message, db_role

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        if not payload.user_id == self.bot.user.id:
            if payload.message_id == 645673197009633298:
                channel, member, message, db_role = await self.get_info(payload)
                for role in channel.guild.roles:
                    if role.name == db_role:
                        await member.add_roles(role)
                        return
                    else:
                        pass
                await message.remove_reaction(payload.emoji, member)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        if not payload.user_id == self.bot.user.id:
            if payload.message_id == 645673197009633298:
                channel, member, message, db_role = await self.get_info(payload)
                for role in channel.guild.roles:
                    if role.name == db_role:
                        await member.remove_roles(role)
                        return
                    else:
                        pass

    @commands.command()
    async def write_message(self, ctx):
        embed = discord.Embed(
            description="\N{REGIONAL INDICATOR SYMBOL LETTER G}\N{REGIONAL INDICATOR SYMBOL LETTER B} Respond to various emojis that match your games to make them your own and make channels visible.\n\n\N{REGIONAL INDICATOR SYMBOL LETTER D}\N{REGIONAL INDICATOR SYMBOL LETTER E} Reagiere mit verschiedene Emojis, die Deinen Spielen entsprechen, um diese zu Deiner Rolle zu machen und entsprechende Channel sichtbar zu machen.\n\n<:R_:626574706086117398> Rockstar-Games\n<:lol:626570323537690657> League of Legends\n<:WT:626572645005262858> War Thunder\n<:BFV:626568991334203412> BATTLEFIELD\n<:borderlands:626571492032774144> Borderlands\n<:R6:626565041818304522> R6:S\n<:csgo:626562550967959560> CS:GO",
            colour=discord.Colour.blurple())
        welcome_channel = discord.utils.get(ctx.guild.channels, name='rollenvergabe')
        message = await welcome_channel.send(embed=embed)
        await message.add_reaction('<:R_:626574706086117398>')
        await message.add_reaction('<:lol:626570323537690657>')
        await message.add_reaction('<:WT:626572645005262858>')
        await message.add_reaction('<:BFV:626568991334203412>')
        await message.add_reaction('<:borderlands:626571492032774144>')
        await message.add_reaction('<:R6:626565041818304522>')
        await message.add_reaction('<:csgo:626562550967959560>')


def setup(bot):
    bot.add_cog(Roles(bot))
