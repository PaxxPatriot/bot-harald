# -*- coding: utf-8 -*-

import time
from datetime import datetime

import discord
from discord.ext import commands, tasks


class Meta(commands.Cog):
    """Commands for utilities related to Discord or the Bot itself."""

    def __init__(self, bot):
        self.bot = bot
        self.autorole.start()

    def cog_unload(self):
        self.autorole.cancel()

    @tasks.loop(hours=1.0)
    async def autorole(self):
        guild = self.bot.get_guild(334353579567742977)
        family = guild.get_role(535903807398215680)
        for member in guild.members:
            date_time_join_difference = abs(member.joined_at - datetime.utcnow())
            if date_time_join_difference.seconds > 5400:
                if member.top_role.is_default():
                    await member.add_roles(family)

    @commands.command()
    async def ping(self, ctx):
        """ Pong! """
        pings = []
        number = 0
        typings = time.monotonic()
        await ctx.trigger_typing()
        typinge = time.monotonic()
        typingms = round((typinge - typings) * 1000)
        pings.append(typingms)
        latencyms = round(self.bot.latency * 1000)
        pings.append(latencyms)
        discords = time.monotonic()
        url = "https://discordapp.com/"
        async with self.bot.session.get(url) as resp:
            if resp.status is 200:
                discorde = time.monotonic()
                discordms = round((discorde-discords)*1000)
                pings.append(discordms)
                discordms = f"{discordms}ms"
            else:
                discordms = "Failed"
        for ms in pings:
            number += ms
        average = round(number / len(pings))
        await ctx.send(f"__**Ping Times:**__\nTyping: `{typingms}ms`  |  Latency: `{latencyms}ms`\nDiscord: `{discordms}`  |  Average: `{average}ms`")

    @commands.command()
    async def join(self, ctx):
        """Tritt einem Server bei."""
        perms = discord.Permissions.none()
        perms.read_messages = True
        perms.external_emojis = True
        perms.send_messages = True
        perms.manage_roles = True
        perms.manage_channels = True
        perms.ban_members = True
        perms.kick_members = True
        perms.manage_messages = True
        perms.embed_links = True
        perms.read_message_history = True
        perms.attach_files = True
        perms.add_reactions = True
        await ctx.send(f'<{discord.utils.oauth_url(self.bot.user.id, perms)}>')


def setup(bot):
    bot.add_cog(Meta(bot))
