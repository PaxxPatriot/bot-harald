# -*- coding: utf-8 -*-

from datetime import datetime

import discord
import pkg_resources
import psutil
from discord.ext import commands


class Stats(commands.Cog):
    """Bot usage statistics."""

    def __init__(self, bot):
        self.bot = bot
        self.process = psutil.Process()

    @commands.command()
    async def about(self, ctx):
        """Tells you information about the bot itself."""

        embed = discord.Embed()
        embed.title = 'Official Bot Server Invite'
        embed.url = 'https://discord.gg/VjXHb33'
        embed.colour = discord.Colour.blurple()

        delta_uptime = datetime.utcnow() - self.bot.uptime
        hours, remainder = divmod(int(delta_uptime.total_seconds()), 3600)
        minutes, seconds = divmod(remainder, 60)
        days, hours = divmod(hours, 24)

        owner = self.bot.get_user(self.bot.owner_id)
        embed.set_author(name=str(owner), icon_url=owner.avatar_url)

        # statistics
        total_members = 0
        total_online = 0
        offline = discord.Status.offline
        for member in self.bot.get_all_members():
            total_members += 1
            if member.status is not offline:
                total_online += 1

        total_unique = len(self.bot.users)

        text = 0
        voice = 0
        guilds = 0
        for guild in self.bot.guilds:
            guilds += 1
            for channel in guild.channels:
                if isinstance(channel, discord.TextChannel):
                    text += 1
                elif isinstance(channel, discord.VoiceChannel):
                    voice += 1

        embed.add_field(
            name='Members', value=f'{total_members} total\n{total_unique} unique\n{total_online} unique online')
        embed.add_field(
            name='Channels', value=f'{text + voice} total\n{text} text\n{voice} voice')

        memory_usage = self.process.memory_full_info().uss / 1024**2
        cpu_usage = self.process.cpu_percent() / psutil.cpu_count()
        embed.add_field(
            name='Process', value=f'{memory_usage:.2f} MiB\n{cpu_usage:.2f}% CPU')

        version = pkg_resources.get_distribution('discord.py').version
        embed.add_field(name='Guilds', value=guilds)
        embed.add_field(
            name="Uptime", value=f"{days}d, {hours}h, {minutes}m, {seconds}s")
        embed.set_footer(
            text=f'Made with discord.py v{version}', icon_url='http://i.imgur.com/5BFecvA.png')
        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Stats(bot))
