# -*- coding: utf-8 -*-

import asyncio
import json
import logging
import random
import re
import sys
import traceback
from datetime import datetime
from logging.handlers import RotatingFileHandler

import aiohttp
import asyncpg
import discord  # Discord.py Bibliothek
from discord.ext import commands

description = """Herzlich Willkommen bei den Fehlschuss Kollegen!"""

initial_extensions = (
    'cogs.meta',
    'cogs.haraldhelp',
    'cogs.admin',
    'cogs.roles',
    'cogs.stats'
)

# Regex Funktionen
playerIdRegex = r"([0-9]+)"

# Logging Konfiguration:
logger = logging.getLogger('discord')
logger.setLevel(logging.INFO)
handler = RotatingFileHandler(
    filename='discord.log', encoding='utf-8', mode='w', maxBytes=1048576, backupCount=32)
handler.setFormatter(logging.Formatter(
    '%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)


def _get_prefix(bot, message):
    prefixes = ['?', '!']
    return commands.when_mentioned_or(*prefixes)(bot, message)


class BotHarald(commands.AutoShardedBot):
    def __init__(self):
        super().__init__(command_prefix=_get_prefix,
                         case_insensitive=True, description=description)
        self.loop = asyncio.get_event_loop()
        self.owner_id = 217021666562146304
        self.session = aiohttp.ClientSession(loop=self.loop)


# Lade config.json
config = json.loads(open('config.json').read())


bot = BotHarald()


def is_channel(channel_id):
    async def predicate(ctx):
        return ctx.channel.id == channel_id
    return commands.check(predicate)


def to_regional(text):
    # assume `text` is all ASCII uppercase, could do checks if necessary / shifting the codepoint
    return ''.join(chr(ord(c) + 0x1F1A5) for c in text)


@bot.command()
async def esl(ctx, *, pLink):
    """Erstellt ein Embed mit Eckdaten zum ESL Play Profil."""
    await ctx.message.delete()
    aJson = None
    playerId = re.search(playerIdRegex, pLink)
    aRequest = f"https://api.eslgaming.com/play/v1/users?ids={playerId.group()}"
    async with aiohttp.ClientSession() as session:
        async with session.get(aRequest) as r:
            aJson = await r.json()
    flag = to_regional(aJson[0]["region"])
    embed = discord.Embed(title="ESL Play", colour=16776969,
                          description=f"[Auf ESL Play anschauen](https://play.eslgaming.com/player/{playerId.group()})")
    embed.add_field(name="Nickname", value=aJson[0]["nickname"], inline=False)
    embed.add_field(name="Region", value=flag, inline=False)
    embed.add_field(name="Awards", value=aJson[0]["awards"], inline=False)
    embed.set_thumbnail(url=aJson[0]["photo"])
    embed.set_footer(
        text="ESL is a brand of Turtle Entertainment GmbH. © 2019.")
    await ctx.send(embed=embed)


@bot.command(name="invite", category="Allgemein")
@is_channel(570304390301220924)
async def _invite(ctx):
    """Erstellt eine Einladung, welche 12 Stunden gültig ist,
    nur ein Mal verwendet werden kann und eine temporäre Mitgliedschaft beinhaltet."""
    invite = await ctx.channel.create_invite(max_age=43200, max_uses=1, temporary=True)
    await ctx.author.send(str(invite))


@_invite.error
async def _invite_error(ctx, error):
    if isinstance(error, commands.CheckFailure):
        await ctx.message.delete()
        await ctx.author.send("Der Command ``!invite`` ist nur im <#570304390301220924> verwendbar.")


@bot.command(category="Allgemein")
@commands.has_any_role("ESL Staff", "Dev", "Lead", "Serveradmin")
async def add_post(ctx, *, argPost):
    """Fügt dem Nachrichtenspeicher eine Nachricht hinzu."""
    query = """INSERT INTO posts (post)
               VALUES ($1);"""
    await bot.db.execute(query, argPost)
    await ctx.send('Post erfolgreich hinzugefügt.')


@bot.command(category="Allgemein")
async def post(ctx):
    """Sendet eine zufällige Nachricht."""
    await ctx.message.delete()
    query = "SELECT COUNT(id) FROM posts;"
    lines = await bot.db.fetchval(query)
    number = random.randint(1, lines-1)
    post = await bot.db.fetchval("SELECT post FROM posts WHERE id = $1;", number)
    await ctx.send(post)


@bot.command(hidden=True)
@commands.is_owner()
async def shutdown(ctx):
    await ctx.send('Bist du sicher, dass du den Bot herunterfahren willst? (ja/nein)')

    def check(m):
        return m.content.lower() == 'ja' or m.content.lower() == 'nein' and m.channel == ctx.channel

    try:
        msg = await bot.wait_for('message', timeout=60.0, check=check)
    except asyncio.TimeoutError:
        await ctx.send('Aktion abgebrochen.')
    else:
        if msg.content.lower() == 'ja':
            await ctx.send('Bot wird heruntergefahren.')
            await bot.db.close()
            await bot.logout()
            exit()
        else:
            await ctx.send('Aktion abgebrochen.')


@bot.event
async def on_ready():
    if not hasattr(bot, 'uptime'):
        bot.uptime = datetime.utcnow()
    for extension in initial_extensions:
        try:
            bot.load_extension(extension)
        except Exception:
            print(f'Failed to load extension {extension}.', file=sys.stderr)
            traceback.print_exc()
    print(f'Ready: {bot.user} (ID: {bot.user.id})')
    await bot.change_presence(activity=discord.Game(name='Counter-Strike: Global Offensive'))
    bot.db = await asyncpg.create_pool(**config["credentials"])

bot.run(config["token"])  # Starte den Bot
